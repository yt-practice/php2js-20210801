const path = require('path')

module.exports = {
	// mode: 'production',
	mode: 'development',
	entry: path.resolve(__dirname, 'src/index.php'),
	module: {
		rules: [{ test: /\.php?$/, use: 'babel-loader' }],
	},
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'index.js',
	},
	node: false,
	resolve: {
		fallback: {
			assert: require.resolve('assert/'),
			path: false,
			fs: false,
		},
	},
}
