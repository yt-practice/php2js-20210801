<?php

if (!function_exists('strcmp')) {
  function strcmp(string $q, string $w) {
    return $q->localeCompare($w);
  }
}

function init() {
  $window->global = $window;
  $buf = require('buffer');
  $window->Buffer = $buf->Buffer;
  $window->process = ['env' => []];
}

init();

$input = $document->querySelector('#input');
$input->value = '<?php\n\nfunction hoge($num) {\n  return $num * 12;\n}\n\nalert(hoge(42));';
$output = $document->querySelector('#output');
$has_error = false;
$runcode = function () use ($window, $output, &$has_error) {
  if ($has_error) return;
  $window->eval($output->value);
};
$transform = function () use ($input, $output, $Babel, &$has_error) {
  try {
    $r = $Babel->transform($input->value, [
      'presets' => [require('babel-preset-php'), require('@babel/preset-flow')],
    ]);
    $output->value = $r->code;
    $has_error = false;
  } catch (\Throwable $th) {
    $console->error($th);
    $mes = $th->stack;
    if (!$mes) $mes = $th->message;
    if (!$mes) $mes = 'Error';
    $output->value = $mes;
    $has_error = true;
  }
};
$input->addEventListener('input', $transform);
$input->addEventListener('keydown', function ($e) use ($runcode) {
  if ($e->isComposing) return;
  if ($e->key === 'Tab') $e->preventDefault();
  if (($e->metaKey || $e->ctrlKey) && $e->key === 'Enter') $runcode();
});
$button = $document->querySelector('button#run');
$button->addEventListener('click', $runcode);
$transform();


// const DATAROOT = 'https://raw.githubusercontent.com/Kengxxiao/ArknightsGameData/master/ja_JP/gamedata';

// function getjson(string $name, $cat = 'excel') {
//   $path = DATAROOT . "/{$cat}/{$name}.json";
//   return fetch($path)->then(function ($r) {
//     return $r->json();
//   });
// }

// getjson('character_table')
//   ->then(function ($data) use ($document, $console) {

//     $operators = [];
//     foreach ($data as $v) {
//       if ($v->appellation && $v->name && $v->itemUsage) $operators[] = $v;
//     }
//     usort($operators, function ($q, $w) {
//       return strcmp($q->appellation, $w->appellation);
//     });
//     foreach ($operators as $v) {
//       $p = $document->createElement('p');
//       $document->body->appendChild($p);
//       $p->textContent = "{$v->name}: {$v->itemUsage}";
//     }
//     $console->log('done');
//   });
